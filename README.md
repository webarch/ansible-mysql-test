# ansible-mysql-test

[![pipeline status](https://git.coop/webarch/ansible-mysql-test/badges/main/pipeline.svg)](https://git.coop/webarch/ansible-mysql-test/-/commits/main)

Repo for testing the Community.Mysql modules, [specifically this issue](https://github.com/ansible-collections/community.mysql/issues/215).

See:

* [.gitlab-ci.yml](.gitlab-ci.yml) file for the tasks that are run
* [local_facts.yml](roles/mariadb/tasks/local_facts.yml) and [mariadb_root.fact.j2](roles/mariadb/templates/mariadb_root.fact.j2) for the task that generate the `ansible_local.mariadb_root.plugin` variable
* [password.yml](roles/mariadb/tasks/password.yml) for the setting and testing of password and `~/.my.cnf` authentication
* [socket.yml](roles/mariadb/tasks/socket.yml) for the setting and testing of socket authentication
